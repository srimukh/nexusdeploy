package com.miracle.test.jmsactivemq.sender;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.stereotype.Component;

@Component
public class Producer{

	private ConnectionFactory factory = null;
	private Connection connection = null;
	private Session session = null;
	private Destination destination = null;
	private MessageProducer producer = null;
	private TextMessage message = null;
	
	public Producer() {

	}
	//This function takes the input from the user through UI and sends the message to queue
	public String sendMessage(String messageText) {

		try {
			//Creating a factory object 
			
			factory = new ActiveMQConnectionFactory("admin","admin","tcp://192.168.1.236:61616");
			//factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
			//Creating a Connection Object - Need to make connection to ActiveMQ
			connection = factory.createConnection();
			connection.start();
			//Creating a session object
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			//Creating a destination queue by name "SAMPLEQUEUE"
			destination = session.createQueue("SAMPLEQUEUE");
			//Creating a producer object 
			producer = session.createProducer(destination);
			//creating a message object
			message = session.createTextMessage();
			message.setText(messageText);
			producer.send(message);

		} catch (JMSException e) {
			e.printStackTrace();
		}
		return "Message has been sent successfully";
	}
}
